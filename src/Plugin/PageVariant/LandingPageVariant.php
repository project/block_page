<?php

/**
 * @file
 * Contains \Drupal\block_page\Plugin\PageVariant\LandingPageVariant.
 */

namespace Drupal\block_page\Plugin\PageVariant;

/**
 * Provides a page variant that serves as a landing page.
 *
 * @PageVariant(
 *   id = "landing",
 *   admin_label = @Translation("Landing page")
 * )
 */
class LandingPageVariant extends DefaultPageVariant {

}
